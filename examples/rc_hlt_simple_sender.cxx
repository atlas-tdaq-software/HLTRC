/*
 * rc_hlt_simple_sender.cxx
 *
 *  Created on: Jun 8, 2012
 *      Author: avolio
 */

#include <stdlib.h>

#include "HLTRC/HLTSender.h"
#include "HLTRC/Command.h"
#include "HLTRC/HLTCommands.h"
#include "HLTRC/Exceptions.h"

#include <string>
#include <iostream>

#include <boost/property_tree/ptree.hpp>

using boost::property_tree::ptree;

int main() {
	try {
		daq::rc::hlt::HLTSender hltSender;

		// Simple example of parameters using the boost property tree
		// We have one parameter group with two multi-value parameters
		ptree paramTree;

		ptree param, param_1, param_2;
		param.put("Name", "PARAMETER GROUP NAME");

		param_1.put("Name", "MULTIVALUE_PARAM_1 NAME");
		param_1.put("Values.Value", "VALUE_1_1");
		param_1.add("Values.Value", "VALUE_1_2");
		param_1.add("Values.Value", "VALUE_1_3");

		param_2.put("Name", "MULTIVALUE_PARAM_2 NAME");
		param_2.put("Values.Value", "VALUE_2_1");
		param_2.add("Values.Value", "VALUE_2_2");
		param_2.add("Values.Value", "VALUE_2_3");

		param.add_child("Values.Param", param_1);
		param.add_child("Values.Param", param_2);

		paramTree.add_child("Configuration.Param", param);

		std::cout << "Sending command CONFIGURE" << std::endl;
		hltSender.send(daq::rc::hlt::Command(daq::rc::hlt::HLTCommands::HLT_CONFIGURE, paramTree));

		std::cout << "Sending command START" << std::endl;
		hltSender.send(daq::rc::hlt::Command(daq::rc::hlt::HLTCommands::HLT_START, paramTree));

                ptree paramTreeFork;
                paramTreeFork.put("Configuration.COMMANDNAME","FORK");
                paramTreeFork.put("Configuration.COUNT","2");
                hltSender.send(daq::rc::hlt::Command(daq::rc::hlt::HLTCommands::HLT_USERCMD,paramTreeFork));

                ptree paramTreeForkAfterCrash;
                paramTreeForkAfterCrash.put("Configuration.COMMANDNAME","FORK_AFTER_CRASH");
                hltSender.send(daq::rc::hlt::Command(daq::rc::hlt::HLTCommands::HLT_USERCMD,paramTreeForkAfterCrash));

		std::cout << "Sending command STOP" << std::endl;
		hltSender.send(daq::rc::hlt::Command(daq::rc::hlt::HLTCommands::HLT_STOP, paramTree));

		std::cout << "Sending command UNCONFIGURE" << std::endl;
		hltSender.send(daq::rc::hlt::Command(daq::rc::hlt::HLTCommands::HLT_UNCONFIGURE, paramTree));

		return EXIT_SUCCESS;
	}
	catch(daq::rc::hlt::HLTSenderError& ex) {
		ers::fatal(ex);
		return EXIT_FAILURE;
	}
}

