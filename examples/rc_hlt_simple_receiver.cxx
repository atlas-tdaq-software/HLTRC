/*
 * rc_hlt_simple_receiver.cxx
 *
 *  Created on: Jun 8, 2012
 *      Author: avolio
 */

#include "HLTRC/HLTReceiver.h"
#include "HLTRC/Exceptions.h"

#include <hltinterface/HLTInterface.h>

#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <memory>

#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include <boost/shared_ptr.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

class MyHLTInterface: public hltinterface::HLTInterface {
    public:
        MyHLTInterface() :
                hltinterface::HLTInterface()
        {
        }

        ~MyHLTInterface() {
        }

        bool prepareWorker(const boost::property_tree::ptree& args) {
            std::cout << "Executing prepareWorker with arguments:\n" << ptree2String(args) << std::endl;
            ::sleep(3);

            return true;
        }

        bool finalizeWorker(const boost::property_tree::ptree& args) {
            std::cout << "Executing finalizeWorker with arguments:\n" << ptree2String(args) << std::endl;
            ::sleep(3);

            return true;
        }

        bool configure(const boost::property_tree::ptree& args) {
            std::cout << "Executing configure with arguments:\n" << ptree2String(args) << std::endl;
            ::sleep(3);

            return true;
        }

        bool connect(const boost::property_tree::ptree& args) {
            std::cout << "Executing connect with arguments:\n" << ptree2String(args) << std::endl;
            ::sleep(3);

            return true;
        }

        bool prepareForRun(const boost::property_tree::ptree& args) {
            std::cout << "Executing prepareForRun with arguments:\n" << ptree2String(args) << std::endl;
            ::sleep(3);

            return true;
        }

        bool stopRun(const boost::property_tree::ptree& args) {
            std::cout << "Executing stopRun with arguments:\n" << ptree2String(args) << std::endl;
            ::sleep(3);

            return true;
        }

        bool disconnect(const boost::property_tree::ptree& args) {
            std::cout << "Executing disconnect with arguments:\n" << ptree2String(args) << std::endl;
            ::sleep(3);

            return true;
        }

        bool unconfigure(const boost::property_tree::ptree& args) {
            std::cout << "Executing unconfigure with arguments:\n" << ptree2String(args) << std::endl;
            ::sleep(3);

            return true;
        }

        bool publishStatistics(const boost::property_tree::ptree& args) {
            std::cout << "Executing publish with arguments:\n" << ptree2String(args) << std::endl;
            ::sleep(3);

            return true;
        }

        void timeOutReached(const boost::property_tree::ptree& args) {
            (void) args;
        }

        bool doEventLoop() {
          return true;
        }

        bool hltUserCommand(const boost::property_tree::ptree& args) {
            std::cout << "Executing user command with arguments:\n" << ptree2String(args) << std::endl;
            ::sleep(3);

            return true;
        }

    private:
        std::string ptree2String(const boost::property_tree::ptree& pt) {
            std::ostringstream conf;
            boost::property_tree::xml_writer_settings<std::string> settings('\t', 1);
            boost::property_tree::xml_parser::write_xml(conf, pt, settings);
            return conf.str();
        }
};

std::unique_ptr<daq::rc::hlt::HLTReceiver> hltRec;

void handler(int signal) {
    (void) signal;
    hltRec->stop();
}

int main() {
    try {
        boost::shared_ptr<hltinterface::HLTInterface> hltInt(new MyHLTInterface());
        hltRec.reset(new daq::rc::hlt::HLTReceiver(hltInt));

        ::signal(SIGINT, handler);
        ::signal(SIGTERM, handler);

        std::cout << "---> Starting the main loop to receive commands...\n";
        hltRec->start();

        std::cout << "---> Receiving loop has been stopped, exiting...\n";

        return EXIT_SUCCESS;
    }
    catch(daq::rc::hlt::HLTReceiverError& ex) {
        ers::fatal(ex);
        return EXIT_FAILURE;
    }
}
