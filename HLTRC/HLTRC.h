/*
The HLTRC inherits from the Controllable interface.  An instance of this class is passed
as a parameter to the ItemCtrl (see HLTRC_main.cxx).  The methods will be called whenever the
Application receives the corrseponding command.

*/

#ifndef RC_HLTRC_H
#define RC_HLTRC_H

#include <string>
#include "RunControl/RunControl.h"
#include "HLTRC/HLTSender.h"

namespace daq {
   namespace rc {
      class HLTRC: public daq::rc::Controllable
     {
     public:
       // override only the needed methods     
       virtual ~HLTRC() noexcept {}
       void configure(const daq::rc::TransitionCmd& cmd) override;
       void prepareForRun(const daq::rc::TransitionCmd& cmd) override;
       void stopRecording(const daq::rc::TransitionCmd& cmd) override;
       void unconfigure(const daq::rc::TransitionCmd& cmd) override;
       void user(const daq::rc::UserCmd& cmd) override;
       void onExit(daq::rc::FSM_STATE) noexcept override;
     private:
       daq::rc::hlt::HLTSender m_hltSender;
       };
   } // rc
} // daq

#endif //RC_HLTRC_H
