/*
 * Command.h
 *
 *  Created on: Mar 29, 2012
 *      Author: avolio
 */

#ifndef _DAQ_RC_HLT_COMMAND_H_
#define _DAQ_RC_HLT_COMMAND_H_

#include <string>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <boost/shared_array.hpp>
#include <boost/property_tree/ptree.hpp>

namespace boost {
	namespace serialization {
		class access;
	}
}

namespace daq {
namespace rc {
namespace hlt {

/**
 * Class implementing a command to be exchanged between the daq::rc::hlt::HLTSender sender and the
 * daq::rc::hlt::HLTReceiver receiver.
 *
 * A command is identified by its name and a set of arguments.
 *
 * @see daq::rc::hlt::HLTSender::send(const Command& cmd)
 */
class Command {
	public:
        /**
         * @brief Constructor
         *
         * It creates an empty command.
         */
		Command();

		/**
		 * @brief Constructor
		 *
		 * @param name The name of the command
		 * @param args The command's arguments
		 */
		Command(const std::string& name, const boost::property_tree::ptree& args);

		/**
		 * @brief Copy constructor
		 */
		Command(const Command& cmd);

		/**
		 * @brief Destructor
		 */
		~Command();

		/**
		 * @brief It returns the command's arguments
		 *
		 * @return The command's arguments
		 */
		const boost::property_tree::ptree& getArguments() const;

		/**
		 * @brief It returns the command's name
		 *
		 * @return The command's name
		 */
		const std::string& getName() const;

		/**
		 * @brief Copy assignment operator
		 */
		Command& operator=(const Command& rhs);

		/**
		 * @brief Equality operator
		 *
		 * Two commands are equal if they have the same name and the same arguments
		 */
		bool operator==(const Command& rhs) const;

		/**
		 * @brief Not-equal operator
		 *
		 * Two commands are not equal if either the name or the arguments are different
		 */
		bool operator!=(const Command& rhs) const;

		/**
		 * @brief It serializes the command in a binary format
		 *
		 * @param cmd The command to serialize
		 * @param size The size (in bytes) of the serialized command
		 *
		 * @throws daq::rc::hlt::SerializationError Some error occurred during the command's serialization
		 */
		static boost::shared_array<char> binarySerialize(const Command& cmd, unsigned int& size);

		/**
		 * @brief It builds a command from its binary serialization
		 *
		 * @param cmd The de-serialized command
		 * @param buffer Buffer holding the serialized command
		 * @param size The size (in bytes) of the buffer
		 *
		 * @throws daq::rc::hlt::SerializationError
		 */
		static void buildFromBinary(Command& cmd, char buffer[], unsigned int size);

	protected:
		friend class boost::serialization::access;

		template<typename Archive>
		void serialize(Archive& ar, const unsigned int version) {
			internal_serialize(ar, version);
		}

		void internal_serialize(boost::archive::binary_oarchive& ar, const unsigned int version);
		void internal_serialize(boost::archive::binary_iarchive& ar, const unsigned int version);

	private:
		std::string m_cmdName;
		boost::property_tree::ptree m_cmdArgs;
};

}
}
}

#endif
