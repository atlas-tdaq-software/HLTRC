/*
 * HLTControlCommand.h
 *
 *  Created on: Apr 11, 2012
 *      Author: avolio
 */

#ifndef _DAQ_RC_HLT_CONTROLCOMMAND_H_
#define _DAQ_RC_HLT_CONTROLCOMMAND_H_

#include "HLTRC/Command.h"

#include <boost/property_tree/ptree.hpp>

namespace daq {
namespace rc {
namespace hlt {
	const Command HLT_CONTROL_CMD("_HLTRC_CONTROL_COMMAND_", boost::property_tree::ptree());
}
}
}

#endif
