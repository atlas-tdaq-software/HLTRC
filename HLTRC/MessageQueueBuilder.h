/*
 * MessageQueueBuilder.h
 *
 *  Created on: Apr 11, 2012
 *      Author: avolio
 */

#ifndef _DAQ_RC_HLT_MESSAGEQUEUEBUILDER_H_
#define _DAQ_RC_HLT_MESSAGEQUEUEBUILDER_H_

#include <memory>
#include <string>
#include <cstdlib>

#include <boost/noncopyable.hpp>

#include <unistd.h>
#include <sys/types.h>


namespace daq {
namespace rc {
namespace hlt {

class CommandCarrier;

enum MessageQueueMode_t {
	OPEN_ONLY,
	CREATE_AND_OPEN
};

// Parameters for the message queue
struct MessageQueueParams {
    // Just open or even create the queue
	MessageQueueMode_t m_open_mode;

	// The ID of the queue
	std::string m_queue_id;

	// The maximum number of messages the queue can hold
	unsigned int m_max_num_msg;

	// The maximum size of a message to be put into the queue
	unsigned int m_max_msg_size;

	// Time to wait for sending a message to the queue: 0 means
	// 'wait for ever' until the queue can accept the message
	unsigned int m_send_timeout;

	// Time to wait for receiving a message from the queue: 0 means
	// 'wait for ever' until the queue is not empty
	unsigned int m_receive_timeout;
};

// Builder for the MessageQueue
class MessageQueueBuilder : private boost::noncopyable {
	public:
		MessageQueueBuilder() {
			m_params.m_open_mode = CREATE_AND_OPEN;
			m_params.m_queue_id = std::string("HLT_MESSAGE_QUEUE") +
			                      "_" +
			                      ((::getenv("TDAQ_PARTITION") == nullptr) ? "" : ::getenv("TDAQ_PARTITION")) +
			                      "_" +
			                      std::to_string(::getuid());
			m_params.m_max_num_msg = 1;
			m_params.m_max_msg_size = 1024*512;
			m_params.m_send_timeout = 0;
			m_params.m_receive_timeout = 1000;
		}

		MessageQueueParams& parameters() {
			return m_params;
		}

	private:
		friend class CommandCarrierFactory;

		std::unique_ptr<CommandCarrier> create() const;
		MessageQueueParams m_params;
};

}
}
}

#endif
