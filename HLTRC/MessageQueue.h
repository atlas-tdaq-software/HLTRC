/*
 * MessageQueue.h
 *
 *  Created on: Apr 11, 2012
 *      Author: avolio
 */

#ifndef _DAQ_RC_HLT_MESSAGEQUEUE_H_
#define _DAQ_RC_HLT_MESSAGEQUEUE_H_

#include "HLTRC/CommandCarrier.h"
#include "HLTRC/MessageQueueBuilder.h"

#include <boost/noncopyable.hpp>
#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

namespace daq {
namespace rc {
namespace hlt {

class Command;

class MessageQueue: public CommandCarrier, private boost::noncopyable {
	public:
		~MessageQueue();

		/**
		 * \throws daq::rc::hlt::MessageQueueError
		 */
		void open();

		void close();

		/**
		 * \throws daq::rc::hlt::MessageQueueError
		 */
		bool send(const Command& cmd);

		/**
		 * \throws daq::rc::hlt::MessageQueueError
		 */
		bool receive(Command& cmd);

	private:
		friend class MessageQueueBuilder;

		explicit MessageQueue(const MessageQueueParams& params);

		const MessageQueueParams m_queue_params;
		boost::shared_ptr<boost::interprocess::message_queue> m_queue;
		boost::mutex m_mutex;
};

}
}
}

#endif
