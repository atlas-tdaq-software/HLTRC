/*
 * HLTSender.h
 *
 *  Created on: Apr 11, 2012
 *      Author: avolio
 */

#ifndef _DAQ_RC_HLT_HLTSENDER_H_
#define _DAQ_RC_HLT_HLTSENDER_H_

#include <memory>
#include <atomic>

#include <boost/noncopyable.hpp>
#include <boost/thread/mutex.hpp>

namespace daq {
namespace rc {
namespace hlt {

class CommandCarrier;
class Command;

/**
 * Class implementing the sender side of the communication between the HLTRC and the HLT applications.
 *
 * @attention For the communication between sender and receiver to work properly, the daq::rc::hlt::HLTSender
 *            should be started first.
 *
 * @note Its is safe to create more than one sender in the same application.
 */
class HLTSender : private boost::noncopyable {
	public:
		/**
		 * @brief Constructor.
		 *
		 * @throws daq::rc::hlt::HLTSenderError The sender could not be created
		 */
		HLTSender();

		/**
		 * @brief Destructor.
		 */
		~HLTSender();

		/**
		 * @brief It sends a command to the receiver.
		 *
		 * This method will not return until the command has been executed by the receiver.
		 *
		 * @param cmd The command to be sent.
		 * @see daq::rc::hlt::HLTSender::start().
		 * @throws daq::rc::hlt::HLTSenderError The command could not be sent.
		 */
		void send(const Command& cmd);

	private:
		std::unique_ptr<CommandCarrier> m_sender;
		static std::atomic<bool> QUEUE_CREATED;
		static boost::mutex MUTEX;
};

}
}
}

#endif
