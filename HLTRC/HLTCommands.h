/*
 * HLTCommands.h
 *
 *  Created on: Jun 14, 2012
 *      Author: avolio
 */

#ifndef _DAQ_RC_HLTCOMMANDS_H_
#define _DAQ_RC_HLTCOMMANDS_H_

#include <string>

namespace daq {
namespace rc {
namespace hlt {

class HLTCommands {
	public:
		static const std::string HLT_CONFIGURE;
		static const std::string HLT_CONNECT;
		static const std::string HLT_START;
		static const std::string HLT_STOP;
		static const std::string HLT_DISCONNECT;
		static const std::string HLT_UNCONFIGURE;
		static const std::string HLT_PUBLISH;
		static const std::string HLT_USERCMD;

	private:
		HLTCommands();
		HLTCommands(const HLTCommands&);
		HLTCommands& operator=(const HLTCommands&);
		~HLTCommands();
};

}
}
}

#endif /* HLTCOMMANDS_H_ */
