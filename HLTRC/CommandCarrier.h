/*
 * CommandCarrier.h
 *
 *  Created on: Apr 11, 2012
 *      Author: avolio
 */

#ifndef _DAQ_RC_HLT_COMMANDCARRIER_H_
#define _DAQ_RC_HLT_COMMANDCARRIER_H_

namespace daq {
namespace rc {
namespace hlt {

class Command;

/**
 * Implementations of this interface are required to throw exceptions inheriting from daq::rc::hlt::CommandCarrierError.
 */

class CommandCarrier {
	public:
		template<typename Params> explicit CommandCarrier(const Params&) {;}
		virtual ~CommandCarrier() {;}

		virtual void open() = 0;
		virtual void close() = 0;
		virtual bool send(const Command& cmd) = 0;
		virtual bool receive(Command& cmd) = 0;
};

}
}
}

#endif
