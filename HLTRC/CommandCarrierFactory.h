/*
 * CommandCarrierFactory.h
 *
 *  Created on: Apr 11, 2012
 *      Author: avolio
 */

#ifndef _DAQ_RC_HLT_COMMANDCARRIERFACTORY_H_
#define _DAQ_RC_HLT_COMMANDCARRIERFACTORY_H_

#include "HLTRC/Exceptions.h"

#include <memory>
#include <exception>

namespace daq {
namespace rc {
namespace hlt {

class CommandCarrier;

class CommandCarrierFactory {
	public:
		/**
		 * \throws daq::rc::hlt::CommandCarrierError
		 */
		template<typename Builder> static std::unique_ptr<CommandCarrier> instance(const Builder& builder) {
			try {
				return std::unique_ptr<CommandCarrier>(builder.create());
			}
			catch(std::exception& ex) {
				const std::string errMsg = std::string("Failed to create the command transport layer: ") + ex.what();
				throw daq::rc::hlt::CommandCarrierError(ERS_HERE, errMsg, ex);
			}
			catch(...) {
				throw daq::rc::hlt::CommandCarrierError(ERS_HERE,
				                                        "Failed to create the command transport layer: unexpected error");
			}
		}

	private:
		CommandCarrierFactory();
		CommandCarrierFactory(const CommandCarrierFactory&);
		CommandCarrierFactory& operator=(const CommandCarrierFactory&);
		~CommandCarrierFactory();
};

}
}
}

#endif
