/*
 * HLTReceiver.h
 *
 *  Created on: Apr 11, 2012
 *      Author: avolio
 */

#ifndef _DAQ_RC_HLT_HLTRECEIVER_H_
#define _DAQ_RC_HLT_HLTRECEIVER_H_

#include <memory>
#include <map>
#include <sys/types.h>
#include <atomic>

#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/function.hpp>
#include <boost/property_tree/ptree.hpp>


namespace hltinterface {
	class HLTInterface;
}

namespace daq {
namespace rc {
namespace hlt {

class Command;
class CommandCarrier;

/**
 * Class implementing the receiver side of the communication between the HLTRC and the HLT applications.
 *
 * @attention For the communication between sender and receiver to work properly, the daq::rc::hlt::HLTSender
 *            should be started first.
 *
 * @attention Only one receiver per application should be created.
 */
class HLTReceiver : private boost::noncopyable {
	public:
		/**
		 * @brief Constructor.
		 *
		 * @param hltInterface Pointer to the hltinterface::HLTInterface implementing the actions to be
		 *                     executed when a command is received.
		 *
		 * @throws daq::rc::hlt::HLTReceiverError The receiver could not be created
		 */
		explicit HLTReceiver(const boost::shared_ptr<hltinterface::HLTInterface>& hltInterface);


		/**
		 * @brief Destructor.
		 */
		~HLTReceiver();

		/**
		 * @brief It starts this receiver: after this call a connection to the sender will be established
		 * and the receiver is ready to receive and execute commands.
		 *
		 * This method blocks until HLTReceiver::stop() is called.
		 *
		 * @attention Do not call this method more than once.
		 *
		 * @throws Any exception thrown by the HLT interface when a command is executed
		 */
		void start();

		/**
		 * @brief It stops the receiver: as a consequence of this call, the HLTReceiver::start() method will return.
		 */
		void stop();

	protected:
		void processCommand(const Command& cmd);
		void initCommandMap();

	private:
		typedef boost::function<bool (const boost::property_tree::ptree&)> hltCmdFn;

		boost::shared_ptr<hltinterface::HLTInterface> m_hlt_interface;
		const pid_t m_pid;
		std::atomic<bool> m_should_stop;
		std::unique_ptr<CommandCarrier> m_receiver;
		std::map<std::string, hltCmdFn> m_command_map;
};

}
}
}

#endif
