/*
The ConfigBuilder class is used by HLTRC to transform the OKS configuration into a ptree.

*/

#ifndef RC_CONFIGBUILDER_H
#define RC_CONFIGBUILDER_H

#include <boost/property_tree/ptree.hpp>

namespace daq {
   namespace rc {
      class ConfigBuilder
     {
     public:
       // override only the needed methods     
       ConfigBuilder(){};
       ~ConfigBuilder() {};
       void buildConfig(boost::property_tree::ptree & ptree);
       void buildRunParams(boost::property_tree::ptree & ptree);
       };
   } // rc
} // daq

#endif //RC_HLTRC_H
