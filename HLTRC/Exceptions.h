/*
 * Exceptions.h
 *
 *  Created on: Jun 19, 2012
 *      Author: avolio
 */

#ifndef _DAQ_RC_HLT_EXCEPTIONS_H_
#define _DAQ_RC_HLT_EXCEPTIONS_H_

#include <ers/ers.h>
#include <iostream>

namespace daq {
namespace rc {
	ERS_DECLARE_ISSUE(hlt,
	                  Exception,
	                  ERS_EMPTY,
	                  ERS_EMPTY
	)

	ERS_DECLARE_ISSUE_BASE(hlt,
	                       SerializationError,
	                       daq::rc::hlt::Exception,
	                       "Serialization failed for command " << cmdName << ": " << reason << " (error code " << code << ")",
	                       ERS_EMPTY,
	                       ((std::string) cmdName)
	                       ((std::string) reason)
	                       ((unsigned int) code)
	)

	ERS_DECLARE_ISSUE_BASE(hlt,
	                       CommandCarrierError,
	                       daq::rc::hlt::Exception,
	                       ERS_EMPTY,
	                       ERS_EMPTY,
	                       ERS_EMPTY
	)

	ERS_DECLARE_ISSUE_BASE(hlt,
	                       MessageQueueError,
	                       daq::rc::hlt::CommandCarrierError,
	                       "Error accessing the message queue \"" << queueName << "\": " << errorDescription,
	                       ERS_EMPTY,
	                       ((std::string) queueName)
	                       ((std::string) errorDescription)
	)

	ERS_DECLARE_ISSUE_BASE(hlt,
	                       NoPeer,
	                       daq::rc::hlt::CommandCarrierError,
	                       ERS_EMPTY,
	                       ERS_EMPTY,
	                       ERS_EMPTY
	)

	ERS_DECLARE_ISSUE_BASE(hlt,
	                       HLTReceiverError,
	                       daq::rc::hlt::Exception,
	                       "HLT receiver problem: " << errorDescription,
	                       ERS_EMPTY,
	                       ((std::string) errorDescription)
	 )

	 ERS_DECLARE_ISSUE_BASE(hlt,
	                        HLTSenderError,
	                        daq::rc::hlt::Exception,
	                        "HLT sender problem: " << errorDescription,
	                        ERS_EMPTY,
	                        ((std::string) errorDescription)
	 )
}
}


#endif /* EXCEPTIONS_H_ */
