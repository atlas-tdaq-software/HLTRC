/*
  Application: rc_build_ptree
  Author: G. Lehmann Miotto
  Date: 6/11/2013
*/
#include <iostream>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "ers/ers.h"
#include "ipc/core.h"
#include "HLTRC/ConfigBuilder.h"
#include "RunControl/RunControl.h"

namespace po = boost::program_options;

namespace daq {
  ERS_DECLARE_ISSUE( rc,
    CantCreateDirectory,
    "Directory '" << directory << "' can not be created",
    ((std::string)directory )
    )
}
int main (int argc, char **argv)
{
  try {
    IPCCore::init(argc, argv);
  }
  catch(daq::ipc::CannotInitialize& e) {
    ers::fatal(e);
    abort();
  }
  catch(daq::ipc::AlreadyInitialized& e) {
    ers::warning(e);
  }

  // take defaults from environment
  std::string tdaq_partition(""), tdaq_db(""), app_name(""), seg_name(""), object_uid(""), file("/tmp/test.txt");
  //  std::string def_p, def_d, def_a, def_l;
  if (getenv("TDAQ_PARTITION"))
     tdaq_partition = getenv("TDAQ_PARTITION");
  if (getenv("TDAQ_DB"))
    tdaq_db = getenv("TDAQ_DB");

  // parse commandline parameters

  po::options_description desc("This application dumps HLT configuration into a ptree");
  desc.add_options()
    ("help,h", "help message")
    ("partition,p",   boost::program_options::value<std::string>(&tdaq_partition)->default_value(tdaq_partition), "partition name (TDAQ_PARTITION)")
    ("database,d",    boost::program_options::value<std::string>(&tdaq_db)->default_value(tdaq_db), "database (TDAQ_DB)")
    ("app_name,n",   boost::program_options::value<std::string>(&app_name), "name of a valid HLTRC application")
    ("seg_name,s",   boost::program_options::value<std::string>(&seg_name), "name of the segment the HLTRC application is in")
    ("object_uid,o", boost::program_options::value<std::string>(&object_uid), "UID of the HLTRCApplication OKS object")
    ("file,f", boost::program_options::value<std::string>(&file)->default_value(file), "file to dump ptree to")
    ("run_params,r","dump IS run parameters instead of OKS configuration")
    ;

  try {
    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::parse_command_line(argc,argv,desc), vm);
    boost::program_options::notify(vm);
    
    if(vm.count("help")) {
        desc.print(std::cout);
        std::cout << std::endl;
        return EXIT_SUCCESS;
    }

    setenv("TDAQ_APPLICATION_OBJECT_ID", object_uid.c_str(), true);
    setenv("TDAQ_APPLICATION_NAME", app_name.c_str(), true);
    setenv("TDAQ_DB", tdaq_db.c_str(), false);
    setenv ("TDAQ_PARTITION", tdaq_partition.c_str(), false);


    boost::filesystem::path path(file);
    try {
      if (!exists(path.parent_path())) {
	boost::filesystem::create_directories(path.parent_path());
	boost::filesystem::permissions(path.parent_path(), boost::filesystem::owner_all | boost::filesystem::group_all);
      } 
    }
    catch (std::exception & ex) {
      ers::fatal(daq::rc::CantCreateDirectory(ERS_HERE, path.parent_path().string(), ex ) );
      return EXIT_FAILURE;
    }
    
    daq::rc::OnlineServices::initialize(tdaq_partition, seg_name);
    
    boost::property_tree::ptree ptree;
    daq::rc::ConfigBuilder cfb;
    if (vm.count("run_params"))
      cfb.buildRunParams(ptree);
    else
      cfb.buildConfig(ptree);
    
    boost::property_tree::write_info(file, ptree);
  
  }
  catch(ers::Issue& ex) {
    std::cout << desc << std::endl;
    std::cout << ex.message() << std::endl;
    ers::fatal(ex);
    return EXIT_FAILURE;
  }
  catch(boost::program_options::error& ex) {
    std::cout << desc << std::endl;
    std::cout << ex.what() << std::endl;
    ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
