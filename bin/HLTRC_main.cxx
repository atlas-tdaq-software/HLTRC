/*
  Application: HLTRC
  Author: G. Lehmann Miotto
  Date: 6/6/2012
*/

#include <boost/program_options.hpp>

#include "ers/ers.h"
#include "ipc/core.h"
#include "HLTRC/HLTRC.h"
#include "RunControl/RunControl.h"

namespace po = boost::program_options;

int main (int argc, char **argv)
{
  try {
    IPCCore::init(argc, argv);
  }
  catch(daq::ipc::CannotInitialize& e) {
    ers::fatal(e);
    abort();
  }
  catch(daq::ipc::AlreadyInitialized& e) {
    ers::warning(e);
  }

  std::string name("");
   
  po::options_description desc("This application controls the HLTPU.");
  try {
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
    po::notify(vm);

    // Parser for the ItemCtrl
    daq::rc::CmdLineParser cmdParser(argc, argv, true);

    // Instntiate ItemCtrl with proper Controllable    
    daq::rc::ItemCtrl itemCtrl(cmdParser, std::shared_ptr<daq::rc::Controllable>(new daq::rc::HLTRC()));
    itemCtrl.init();
    itemCtrl.run();
  }
  catch(daq::rc::CmdLineHelp& ex) {
    std::cout << desc << std::endl;
    std::cout << ex.message() << std::endl;
  }
  catch(ers::Issue& ex) {
    ers::fatal(ex);
    return EXIT_FAILURE;
  }
  catch(boost::program_options::error& ex) {
    ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
