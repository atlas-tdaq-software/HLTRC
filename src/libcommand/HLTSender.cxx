/*
 * HLTSender.cpp
 *
 *  Created on: Apr 11, 2012
 *      Author: avolio
 */

#include "HLTRC/HLTSender.h"
#include "HLTRC/HLTControlCommand.h"
#include "HLTRC/MessageQueueBuilder.h"
#include "HLTRC/Command.h"
#include "HLTRC/CommandCarrier.h"
#include "HLTRC/CommandCarrierFactory.h"
#include "HLTRC/Exceptions.h"

std::atomic<bool> daq::rc::hlt::HLTSender::QUEUE_CREATED;
boost::mutex daq::rc::hlt::HLTSender::MUTEX;

daq::rc::hlt::HLTSender::HLTSender() {
	// Here we want to give the possibility to have multiple HLTSender in
	// the same application. So only the first one creates and opens the queue
	// (the creation implies the removal of an already existing queue), while
	// the subsequent ones are limited to opening the queue.

	daq::rc::hlt::MessageQueueBuilder builder;

	try {
		if(daq::rc::hlt::HLTSender::QUEUE_CREATED == false) {
			// Lock the mutex: double check-like algorithm
			boost::mutex::scoped_lock lk(daq::rc::hlt::HLTSender::MUTEX);

			if(daq::rc::hlt::HLTSender::QUEUE_CREATED == false) {
				builder.parameters().m_open_mode = CREATE_AND_OPEN;
				m_sender = daq::rc::hlt::CommandCarrierFactory::instance(builder);
				m_sender->open();
				daq::rc::hlt::HLTSender::QUEUE_CREATED = true;
			} else {
				// Here the mutex can be unlocked, nothing more to protect (i.e., QUEUE_CREATED)
				lk.unlock();

				builder.parameters().m_open_mode = OPEN_ONLY;
				m_sender = daq::rc::hlt::CommandCarrierFactory::instance(builder);
				m_sender->open();
			}
		} else {
			builder.parameters().m_open_mode = OPEN_ONLY;
			m_sender = daq::rc::hlt::CommandCarrierFactory::instance(builder);
			m_sender->open();
		}
	}
	catch(daq::rc::hlt::CommandCarrierError& ex) {
		const std::string errDescr = std::string("the HLTSender cannot be created (") + ex.message() + ")";
		throw daq::rc::hlt::HLTSenderError(ERS_HERE, errDescr, ex);
	}
}

daq::rc::hlt::HLTSender::~HLTSender() {
	m_sender->close();
}

void daq::rc::hlt::HLTSender::send(const daq::rc::hlt::Command& cmd) {
	try {
		m_sender->send(cmd);

		// Command sent, now waiting to be executed
		m_sender->send(daq::rc::hlt::HLT_CONTROL_CMD);
		m_sender->send(daq::rc::hlt::HLT_CONTROL_CMD);
	}
	catch(daq::rc::hlt::HLTSenderError& ex) {
		const std::string errDescr = std::string("command \"") + cmd.getName() + "\" cannot be sent (" + ex.message()
		        + ")";
		throw daq::rc::hlt::HLTSenderError(ERS_HERE, errDescr, ex);
	}
}
