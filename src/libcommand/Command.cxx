/*
 * Command.cpp
 *
 *  Created on: Mar 29, 2012
 *      Author: avolio
 */

#include "HLTRC/Command.h"
#include "HLTRC/Exceptions.h"

#include <string.h>

#include <sstream>

#include <boost/archive/archive_exception.hpp>

#include <boost/serialization/access.hpp>
#include <boost/serialization/string.hpp>
#include <boost/property_tree/ptree_serialization.hpp>

daq::rc::hlt::Command::Command() {
}

daq::rc::hlt::Command::Command(const std::string& name, const boost::property_tree::ptree& args) :
		m_cmdName(name), m_cmdArgs(args)
{
}

daq::rc::hlt::Command::Command(const daq::rc::hlt::Command& cmd) :
		m_cmdName(cmd.m_cmdName), m_cmdArgs(cmd.m_cmdArgs)
{
}

daq::rc::hlt::Command::~Command() {
}

const boost::property_tree::ptree& daq::rc::hlt::Command::getArguments() const {
	return m_cmdArgs;
}

const std::string& daq::rc::hlt::Command::getName() const {
	return m_cmdName;
}

daq::rc::hlt::Command& daq::rc::hlt::Command::operator=(const daq::rc::hlt::Command& rhs) {
	m_cmdName = rhs.m_cmdName;
	m_cmdArgs = rhs.m_cmdArgs;

	return *this;
}

bool daq::rc::hlt::Command::operator==(const daq::rc::hlt::Command& rhs) const {
	return ((m_cmdName == rhs.m_cmdName) && (m_cmdArgs == rhs.m_cmdArgs));
}

bool daq::rc::hlt::Command::operator!=(const daq::rc::hlt::Command& rhs) const {
	return ((m_cmdName != rhs.m_cmdName) || (m_cmdArgs != rhs.m_cmdArgs));
}

boost::shared_array<char> daq::rc::hlt::Command::binarySerialize(const daq::rc::hlt::Command& cmd, unsigned int& size) {
	try {
		// Open also in "in" mode because then we have to read the bytes from
		// the buffer in order to return to the caller
		std::stringbuf o_buff(std::ios_base::out | std::ios_base::in | std::ios_base::binary);
		boost::archive::binary_oarchive o_arch(o_buff);
		o_arch << cmd;

		// This buffer will contain the serialized command
		boost::shared_array<char> b;

		// Reset the internal pointer to the beginning of the buffer
		// so that the next call returns the real number of available characters
		// The 'pubseekpos' call fails if the stream is not opened also as "in"
		const int pos = o_buff.pubseekpos(0, std::ios_base::in);
		if(pos == 0) {
			const int len = o_buff.in_avail();
			if(len > 0) {
				b.reset(new char[len]);

				const int read = o_buff.sgetn(b.get(), len);
				if(read == len) {
					size = read;
				} else {
					std::ostringstream errMsg;
					errMsg << "Error reading from the serialization buffer: expected to read " << len
					        << " bytes, but actually read " << read << " bytes";
					throw daq::rc::hlt::SerializationError(ERS_HERE, cmd.getName(), errMsg.str(), 0);
				}
			} else {
				throw daq::rc::hlt::SerializationError(ERS_HERE,
				                                       cmd.getName(),
				                                       "Cannot read from the serialization buffer",
				                                       0);
			}
		} else {
			throw daq::rc::hlt::SerializationError(ERS_HERE,
			                                       cmd.getName(),
			                                       "Cannot reset the reader pointer of the serialization buffer",
			                                       0);
		}

		return b;
	}
	catch(boost::archive::archive_exception& ex) {
		throw daq::rc::hlt::SerializationError(ERS_HERE, cmd.getName(), ex.what(), ex.code, ex);
	}
}

void daq::rc::hlt::Command::buildFromBinary(daq::rc::hlt::Command& cmd, char buffer[], unsigned int size) {
	std::stringbuf i_buff(std::ios_base::in | std::ios_base::binary);
	i_buff.pubsetbuf(buffer, size);

	try {
		boost::archive::binary_iarchive i_arch(i_buff);
		i_arch >> cmd;
	}
	catch(boost::archive::archive_exception& ex) {
		throw daq::rc::hlt::SerializationError(ERS_HERE, cmd.getName(), ex.what(), ex.code);
	}
}

void daq::rc::hlt::Command::internal_serialize(boost::archive::binary_oarchive& ar, const unsigned int version) {
	(void) version;
	ar & this->m_cmdName & this->m_cmdArgs;
}

void daq::rc::hlt::Command::internal_serialize(boost::archive::binary_iarchive& ar, const unsigned int version) {
	(void) version;
	ar & this->m_cmdName & this->m_cmdArgs;
}

