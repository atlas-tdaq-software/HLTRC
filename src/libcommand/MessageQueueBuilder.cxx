/*
 * MessageQueueBuilder.cxx
 *
 *  Created on: Apr 11, 2012
 *      Author: avolio
 */

#include "HLTRC/MessageQueueBuilder.h"
#include "HLTRC/MessageQueue.h"

std::unique_ptr<daq::rc::hlt::CommandCarrier> daq::rc::hlt::MessageQueueBuilder::create() const {
	return std::unique_ptr<daq::rc::hlt::CommandCarrier>(new daq::rc::hlt::MessageQueue(this->m_params));
}

