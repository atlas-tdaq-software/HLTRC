/*
 * MessageQueue.cpp
 *
 *  Created on: Apr 11, 2012
 *      Author: avolio
 */

#include "HLTRC/MessageQueue.h"
#include "HLTRC/Command.h"
#include "HLTRC/Exceptions.h"

#include <boost/shared_array.hpp>
#include <boost/scoped_array.hpp>
#include <boost/thread/thread_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/interprocess/exceptions.hpp>


daq::rc::hlt::MessageQueue::MessageQueue(const daq::rc::hlt::MessageQueueParams& params) :
		CommandCarrier(params), m_queue_params(params), m_queue()
{
}

daq::rc::hlt::MessageQueue::~MessageQueue() {
}

void daq::rc::hlt::MessageQueue::open() {
	// Boost shared pointers cannot be read and written at the same time
	boost::mutex::scoped_lock lk(m_mutex);

	if(m_queue.get() == 0) {
		try {
			switch(m_queue_params.m_open_mode) {
				case OPEN_ONLY:
				{
					m_queue.reset(new boost::interprocess::message_queue(boost::interprocess::open_only,
					                                                     m_queue_params.m_queue_id.c_str()));
					break;
				}
				case CREATE_AND_OPEN:
				{
				    boost::interprocess::permissions perm;
				    perm.set_unrestricted();

					boost::interprocess::message_queue::remove(m_queue_params.m_queue_id.c_str());
					m_queue.reset(new boost::interprocess::message_queue(boost::interprocess::open_or_create,
					                                                     m_queue_params.m_queue_id.c_str(),
					                                                     m_queue_params.m_max_num_msg,
					                                                     m_queue_params.m_max_msg_size,
					                                                     perm));
					break;
				}
				default:
				{
					throw daq::rc::hlt::MessageQueueError(ERS_HERE,
					                                      m_queue_params.m_queue_id,
					                                      "the queue cannot be opened because the open mode has not been defined");
					break;
				}
			}
		}
		catch(boost::interprocess::interprocess_exception& ex) {
		    const boost::interprocess::error_code_t ec = ex.get_error_code();
		    if((m_queue_params.m_open_mode == OPEN_ONLY) &&
		       ((ec == boost::interprocess::not_found_error) || (ec == boost::interprocess::not_such_file_or_directory)))
		    {
		        throw daq::rc::hlt::NoPeer(ERS_HERE);
		    }

			const std::string errDescr = std::string("the queue cannot be opened (reason: ") + ex.what() + ")";
			throw daq::rc::hlt::MessageQueueError(ERS_HERE, m_queue_params.m_queue_id, errDescr, ex);
		}
	} else {
		throw daq::rc::hlt::MessageQueueError(ERS_HERE,
		                                      m_queue_params.m_queue_id,
		                                      "the queue cannot be opened because it is already open");
	}
}

void daq::rc::hlt::MessageQueue::close() {
    if(m_queue_params.m_open_mode == CREATE_AND_OPEN) {
        boost::interprocess::message_queue::remove(m_queue_params.m_queue_id.c_str());
    }

    // Boost shared pointers cannot be read and written at the same time
	boost::mutex::scoped_lock lk(m_mutex);
	m_queue.reset();
}

bool daq::rc::hlt::MessageQueue::send(const daq::rc::hlt::Command& cmd) {
	boost::shared_ptr<boost::interprocess::message_queue> mq;
	{
		// Boost shared pointers cannot be read and written at the same time
		boost::mutex::scoped_lock lk(m_mutex);
		mq = m_queue;
	}

	if(mq.get() != 0) {
		try {
			bool messageSent = true;

			unsigned int size = 0;
			boost::shared_array<char> sa(daq::rc::hlt::Command::binarySerialize(cmd, size));

			if(m_queue_params.m_send_timeout == 0) {
				mq->send(sa.get(), size, 0);
			} else {
				const boost::system_time start = boost::get_system_time();
				const boost::posix_time::milliseconds delay(m_queue_params.m_send_timeout);
				messageSent = mq->timed_send(sa.get(), size, 0, start + delay);
			}

			return messageSent;
		}
		catch(daq::rc::hlt::SerializationError& ex) {
			const std::string errDescr = std::string("cannot send the command \"") + cmd.getName() + "\" to the queue because of a problem with the message serialization: " + ex.message();
			throw daq::rc::hlt::MessageQueueError(ERS_HERE, m_queue_params.m_queue_id, errDescr, ex);
		}
		catch(boost::interprocess::interprocess_exception& ex) {
			const std::string errDescr = std::string("cannot send the command \"") + cmd.getName() + "\" to the queue: " + ex.what();
			throw daq::rc::hlt::MessageQueueError(ERS_HERE, m_queue_params.m_queue_id, errDescr, ex);
		}
	} else {
		const std::string errDescr = std::string("cannot send the command \"") + cmd.getName() + "\" to the queue because it is not open yet or it has already been closed";
		throw daq::rc::hlt::MessageQueueError(ERS_HERE, m_queue_params.m_queue_id, errDescr);
	}
}

bool daq::rc::hlt::MessageQueue::receive(daq::rc::hlt::Command& cmd) {
	boost::shared_ptr<boost::interprocess::message_queue> mq;
	{
		// Boost shared pointers cannot be read and written at the same time
		boost::mutex::scoped_lock lk(m_mutex);
		mq = m_queue;
	}

	if(mq.get() != 0) {
		try {
			bool messageReceived = true;

			unsigned int msg_prio = 0;
			boost::interprocess::message_queue::size_type msg_size = 0;
			const unsigned int max_msg_size = mq->get_max_msg_size();
			boost::scoped_array<char> buffer(new char[max_msg_size]);

			if(m_queue_params.m_receive_timeout == 0) {
				mq->receive(buffer.get(), max_msg_size, msg_size, msg_prio);
			} else {
				const boost::system_time start = boost::get_system_time();
				const boost::posix_time::milliseconds delay(m_queue_params.m_receive_timeout);

				messageReceived = mq->timed_receive(buffer.get(), max_msg_size, msg_size, msg_prio, start + delay);
			}

			if(messageReceived == true) {
				daq::rc::hlt::Command::buildFromBinary(cmd, buffer.get(), max_msg_size);
			}

			return messageReceived;
		}
		catch(daq::rc::hlt::SerializationError& ex) {
			const std::string errDescr = std::string("error receiving the command \"") + cmd.getName() + "\" because of a problem with the command serialization: " + ex.message();
			throw daq::rc::hlt::MessageQueueError(ERS_HERE, m_queue_params.m_queue_id, errDescr, ex);
		}
		catch(boost::interprocess::interprocess_exception& ex) {
			const std::string errDescr = std::string("error receiving the command \"") + cmd.getName() + "\": " + ex.what();
			throw daq::rc::hlt::MessageQueueError(ERS_HERE, m_queue_params.m_queue_id, errDescr, ex);
		}
	} else {
		const std::string errDescr = std::string("cannot receive the command \"") + cmd.getName() + "\" because the queue is not open yet or it has already been closed";
		throw daq::rc::hlt::MessageQueueError(ERS_HERE, m_queue_params.m_queue_id, errDescr);
	}
}
