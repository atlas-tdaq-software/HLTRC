/*
 * HLTReceiver.cpp
 *
 *  Created on: Apr 11, 2012
 *      Author: avolio
 */

#include "HLTRC/HLTReceiver.h"
#include "HLTRC/HLTControlCommand.h"
#include "HLTRC/MessageQueueBuilder.h"
#include "HLTRC/Command.h"
#include "HLTRC/HLTCommands.h"
#include "HLTRC/CommandCarrier.h"
#include "HLTRC/CommandCarrierFactory.h"
#include "HLTRC/Exceptions.h"

#include <hltinterface/HLTInterface.h>

#include <unistd.h>

#include <boost/thread/thread_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/bind/bind.hpp>

const std::string daq::rc::hlt::HLTCommands::HLT_CONFIGURE = "HLT_CONFIGURE";
const std::string daq::rc::hlt::HLTCommands::HLT_CONNECT = "HLT_CONNECT";
const std::string daq::rc::hlt::HLTCommands::HLT_START = "HLT_START";
const std::string daq::rc::hlt::HLTCommands::HLT_STOP = "HLT_STOP";
const std::string daq::rc::hlt::HLTCommands::HLT_DISCONNECT = "HLT_DISCONNECT";
const std::string daq::rc::hlt::HLTCommands::HLT_UNCONFIGURE = "HLT_UNCONFIGURE";
const std::string daq::rc::hlt::HLTCommands::HLT_PUBLISH = "HLT_PUBLISH";
const std::string daq::rc::hlt::HLTCommands::HLT_USERCMD = "HLT_USERCMD";


daq::rc::hlt::HLTReceiver::HLTReceiver(const boost::shared_ptr<hltinterface::HLTInterface>& hltInterface) :
		m_hlt_interface(hltInterface), m_pid(::getpid()), m_should_stop()
{
	daq::rc::hlt::MessageQueueBuilder builder;
	builder.parameters().m_open_mode = OPEN_ONLY;

	try {
		m_receiver = daq::rc::hlt::CommandCarrierFactory::instance(builder);
	}
	catch(daq::rc::hlt::CommandCarrierError& ex) {
		const std::string errDescr = std::string("the HLTReceiver cannot be created (") + ex.message() + ")";
		throw daq::rc::hlt::HLTReceiverError(ERS_HERE, errDescr, ex);
	}

	initCommandMap();

	// This is not here by chance; let's use the atomic as
	// a memory barrier so that we are sure that the receiver
	// thread sees the right status of the receiver
	m_should_stop = false;
}

daq::rc::hlt::HLTReceiver::~HLTReceiver() {
	m_receiver->close();
}

void daq::rc::hlt::HLTReceiver::stop() {
	m_should_stop = true;
}

void daq::rc::hlt::HLTReceiver::processCommand(const daq::rc::hlt::Command& cmd) {
	const std::string& cmdName = cmd.getName();
	const std::map<std::string, hltCmdFn>::const_iterator& it = m_command_map.find(cmdName);
	if(it != m_command_map.end()) {
		const boost::property_tree::ptree& args = cmd.getArguments();
		(it->second)(args);
	} else {
		std::string msg = std::string("Command ") + cmdName + std::string(" is ignored");
		ERS_LOG(msg);
	}
}

void daq::rc::hlt::HLTReceiver::start() {
    bool receiverOpened = false;
    bool firstAttempt = true;

	while(m_should_stop == false && ::getpid() == m_pid) {
		try {
	        // Bad work-around in order to avoid start dependencies between the sender and the receiver
	        if(receiverOpened == false) {
	            try {
	                m_receiver->open();
	                receiverOpened = true;

	                ERS_LOG("Connection with the command sender has been established");
	            }
	            catch(daq::rc::hlt::NoPeer& ex) {
	                // The call on the receiver failed because the other side was not open yet: sleep and try again
	                if(firstAttempt == true) {
	                    ERS_LOG("The command sender is not ready yet, I will keep trying opening the connection");
	                    firstAttempt = false;
	                }

	                ::usleep(100000);
	                continue;
	            }
	            catch(daq::rc::hlt::CommandCarrierError& ex) {
	                ers::fatal(ex);
	                break;
	            }
	        }

	        // Here is the real receiving part
			daq::rc::hlt::Command cmd;
			bool received = m_receiver->receive(cmd);
			if((received == true) && (cmd != daq::rc::hlt::HLT_CONTROL_CMD)) {
			    // This may throw
			    processCommand(cmd);
			}
		}
		catch(daq::rc::hlt::CommandCarrierError& ex) {
			// Error from the 'receive' method
			const std::string errDescr = std::string("error receiving a command (") + ex.message() + ")";
			daq::rc::hlt::HLTReceiverError issue(ERS_HERE, errDescr, ex);

			ers::fatal(issue);
		}
	}

	if(::getpid() != m_pid) {
		ERS_LOG("HLT receiver exiting: I am running in a forked child");
	}
}

void daq::rc::hlt::HLTReceiver::initCommandMap() {
        using namespace boost::placeholders;

	// TODO: look for some other solution allowing some automatic filling of this map
	m_command_map.insert(std::make_pair(daq::rc::hlt::HLTCommands::HLT_CONFIGURE,
	                                    boost::bind(&hltinterface::HLTInterface::configure, m_hlt_interface, _1)));

	m_command_map.insert(std::make_pair(daq::rc::hlt::HLTCommands::HLT_CONNECT,
	                                    boost::bind(&hltinterface::HLTInterface::connect, m_hlt_interface, _1)));

	m_command_map.insert(std::make_pair(daq::rc::hlt::HLTCommands::HLT_START,
	                                    boost::bind(&hltinterface::HLTInterface::prepareForRun, m_hlt_interface, _1)));

	m_command_map.insert(std::make_pair(daq::rc::hlt::HLTCommands::HLT_STOP,
	                                    boost::bind(&hltinterface::HLTInterface::stopRun, m_hlt_interface, _1)));

	m_command_map.insert(std::make_pair(daq::rc::hlt::HLTCommands::HLT_DISCONNECT,
	                                    boost::bind(&hltinterface::HLTInterface::disconnect, m_hlt_interface, _1)));

	m_command_map.insert(std::make_pair(daq::rc::hlt::HLTCommands::HLT_UNCONFIGURE,
	                                    boost::bind(&hltinterface::HLTInterface::unconfigure, m_hlt_interface, _1)));

	m_command_map.insert(std::make_pair(daq::rc::hlt::HLTCommands::HLT_PUBLISH,
	                                    boost::bind(&hltinterface::HLTInterface::publishStatistics, m_hlt_interface, _1)));

	m_command_map.insert(std::make_pair(daq::rc::hlt::HLTCommands::HLT_USERCMD,
	                                    boost::bind(&hltinterface::HLTInterface::hltUserCommand, m_hlt_interface, _1)));
}
