/**
 * File: HLTRC.cxx
 * This is an implementation of the Controllable interface
 * It reads comfiguration parameters from OKS and passes them on the HLT
 * Author: G.a Lehmann Miotto
 * Date: 8.6.2012 Creation
 **/

#include <iostream>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <ers/ers.h>
#include <eformat/eformat.h>
#include <config/Configuration.h>
#include <dal/BaseApplication.h>
#include <dal/Partition.h>
#include <dal/ResourceBase.h>
#include <dal/ResourceSet.h>
#include <dal/RunControlApplicationBase.h>
#include <dal/Segment.h>
#include <dal/Detector.h>
#include <dal/HLTImplementation.h>
#include <dal/TriggerDBConnection.h>
#include <dal/TriggerConfiguration.h>
#include <dal/L1TriggerConfiguration.h>

/*
#include <DFdal/PTApplicationBase.h>
#include <DFdal/PtioImplementation.h>
#include <DFdal/ptioEfd.h>
#include <DFdal/ROSE.h>
*/
#include <DFdal/ROS.h>
#include <DFdal/InputChannel.h>
#include <DFdal/DFParameters.h>
#include <DFdal/CircBuffer.h>

#include <HLTPUDal/HLTImplementationJobOptions.h>
#include <HLTPUDal/HLTImplementationDB.h>
#include <HLTPUDal/HLTImplementationDBPython.h>
#include <HLTPUDal/HLTCommonParameters.h>
#include <HLTPUDal/HLTDataSourceImpl.h>
#include <HLTPUDal/HLTMonInfoImpl.h>
#include <HLTPUDal/HLTRCApplication.h>
#include <HLTPUDal/HLTDFDummyBackend.h>
#include <HLTPUDal/HLTDFDCMBackend.h>
#include <HLTPUDal/HLTDFFileBackend.h>
#include <HLTPUDal/ROSLatencyMeasurementParams.h>

#include "pudummydal/DataAccess.h"         
#include "pudummydal/DummyROBStep.h"  
#include "pudummydal/DummyStreamTag.h"
#include "pudummydal/DummyCalStreamTag.h"  
#include "pudummydal/DummyStep.h"     
#include "pudummydal/PuDummySteering.h"

#include "monsvcdal/PublishingApplication.h"
#include "monsvcdal/ConfigurationRuleBundle.h"
#include "monsvcdal/ConfigurationRule.h"
#include "monsvcdal/PublishingParameters.h"
#include "monsvcdal/OstreamPublishingParameters.h"
#include "monsvcdal/ISPublishingParameters.h"
#include "monsvcdal/OHPublishingParameters.h"

#include "dcm/dal/DcmApplication.h"
#include "dcm/dal/DcmProcessor.h"
#include <system/Host.h>
#include "dal/Computer.h"

#include <rc/RunParamsNamed.h>
#include <ddc/DdcFloatInfoNamed.h>
#include <eformat/SourceIdentifier.h>

#include "HLTRC/ConfigBuilder.h"
#include "RunControl/RunControl.h"

namespace daq {
  ERS_DECLARE_ISSUE_BASE( rc,
                          CommandForwarding,
                          rc::Exception,
                          "Failed to forward command: " << reason,
                          ERS_EMPTY,
                          ((std::string) reason)
                          )
}

using namespace boost::property_tree;

// Helper to Configure: Add a Dummy[Cal]StreamTag object to the ptree configuration.
static void createStreamTag(ptree & pt, const daq::df::DummyStreamTag * st)
{
  pt.add("UID", st->UID());
  pt.add("name", st->get_name());
  pt.add("probability", st->get_probability());
  if (const daq::df::DummyCalStreamTag * cst = st->cast<daq::df::DummyCalStreamTag >()) {
    pt.add("calibData", cst->get_calibData()->UID());
  }
}

// Helper to Configure: Add a DataAccess object to the ptree configuration.
static void add_DataAccess(const daq::df::DataAccess * da, ptree & dataAccess)
{
  // Create new DataAccess object
  dataAccess.add("UID", da->UID());
  dataAccess.add("subDetector", da->get_subDetector());
  dataAccess.add("nROBDistribution", da->get_nROBDistribution());
  dataAccess.add("comment", da->get_comment());
}

static void add_ConfigurationRules(const monsvc::dal::ConfigurationRuleBundle * cb, ptree &crb) {

  crb.add("UID", cb->UID());
  for (const auto &a : cb->get_Rules()) {
    ptree ptr;
    ptr.add("UID", a->UID());
    ptr.add("IncludeFilter", a->get_IncludeFilter());
    ptr.add("ExcludeFilter", a->get_ExcludeFilter());
    ptr.add("Name", a->get_Name());
    ptr.add("Parameters."+ a->get_Parameters()->class_name()+ ".UID",a->get_Parameters()->UID() );
    if (const monsvc::dal::OstreamPublishingParameters *b = a->get_Parameters()->cast<monsvc::dal::OstreamPublishingParameters>()) {
      ptr.add("Parameters."+ a->get_Parameters()->class_name()+ ".OutputStream", b->get_OutputStream());
    }
    else if  (const monsvc::dal::ISPublishingParameters *b = a->get_Parameters()->cast<monsvc::dal::ISPublishingParameters>()) {
      ptr.add("Parameters."+ a->get_Parameters()->class_name()+ ".PublishInterval", b->get_PublishInterval());
      ptr.add("Parameters."+ a->get_Parameters()->class_name()+ ".NumberOfSlots", b->get_NumberOfSlots());
      ptr.add("Parameters."+ a->get_Parameters()->class_name()+ ".ISServer", b->get_ISServer());

    }
    else if (const monsvc::dal::OHPublishingParameters *b = a->get_Parameters()->cast<monsvc::dal::OHPublishingParameters>()) {
      ptr.add("Parameters."+ a->get_Parameters()->class_name()+ ".PublishInterval", b->get_PublishInterval());
      ptr.add("Parameters."+ a->get_Parameters()->class_name()+ ".OHServer", b->get_OHServer());
      ptr.add("Parameters."+ a->get_Parameters()->class_name()+ ".NumberOfSlots", b->get_NumberOfSlots());
      ptr.add("Parameters."+ a->get_Parameters()->class_name()+ ".ROOTProvider", b->get_ROOTProvider());
    }

    crb.add_child("Rules.ConfigurationRule", ptr);
  }

  for (const auto & a: cb->get_LinkedBundles()) {
    ptree pt;
    add_ConfigurationRules(a, pt);
    crb.add_child("LinkedBundles."+a->class_name(), pt);
  }
}

/** 
 *configure method: all parameters useful to the HLT are extracted from OKS and formatted into a command before sending the command to the child HLT
 **/
void daq::rc::ConfigBuilder::buildConfig(ptree & configTree) {
  //ptree configTree; // tree of configuration parameters

  // Extract IO parameters for communication to DCM
  const daq::hlt::HLTRCApplication * app = OnlineServices::instance().getApplication().cast<daq::hlt::HLTRCApplication>();
  if(!app)
    throw (daq::rc::ConfigurationIssue(ERS_HERE, "This is not a HLTRCApplication"));

  // Get ROSs and ROBs
  // get all ROSs, ROSEs and pROSs from in the partition

  const daq::core::Partition& tdaq_partition = OnlineServices::instance().getPartition();

  // Temporary definition of event timeout; will come from DFParameters
  int eventTimeout = 180;
  int maximumHltResultMb = 0;
  std::stringstream et;
  if(getenv("TDAQ_EVENT_TIMEOUT")) {
    et << getenv("TDAQ_EVENT_TIMEOUT");
    et >> eventTimeout;
  }

  ptree dfParams;
  dfParams.add("UID", tdaq_partition.get_DataFlowParameters()->UID());
  dfParams.add("EventTimeout", eventTimeout);

  // Now Trigger config
  const daq::core::TriggerConfiguration* trigger_config = tdaq_partition.get_TriggerConfiguration();
  if(!trigger_config)
    throw (daq::rc::ConfigurationIssue(ERS_HERE, "This is no TriggerConfiguration associated to the Partition"));
    
  ptree triggerConfiguration;
  const daq::core::HLTImplementation* hlt = trigger_config->get_hlt();
  if(!hlt)
    throw (daq::rc::ConfigurationIssue(ERS_HERE, "Could not find an HLT configuration"));
  
  triggerConfiguration.add("UID", trigger_config->UID());
  if(trigger_config->get_l1()) {
    ptree l1;
    l1.add("UID", trigger_config->get_l1()->UID());
    l1.add("Lvl1PrescaleKey", trigger_config->get_l1()->get_Lvl1PrescaleKey());
    l1.add("Lvl1BunchGroupKey", trigger_config->get_l1()->get_Lvl1BunchGroupKey());
    l1.add("ConfigureLvl1MenuFrom", trigger_config->get_l1()->get_ConfigureLvl1MenuFrom());
    triggerConfiguration.add_child("L1TriggerConfiguration."+trigger_config->get_l1()->class_name(), l1);
  }
  //  triggerConfiguration.add("l2", hlt->UID());

  if(trigger_config->get_TriggerDBConnection()) {
    ptree tdbconn;
    tdbconn.add("UID", trigger_config->get_TriggerDBConnection()->UID());
    tdbconn.add("Server", trigger_config->get_TriggerDBConnection()->get_Server());
    tdbconn.add("Port", trigger_config->get_TriggerDBConnection()->get_Port());
    tdbconn.add("Name", trigger_config->get_TriggerDBConnection()->get_Name());
    tdbconn.add("Alias",  trigger_config->get_TriggerDBConnection()->get_Alias());
    tdbconn.add("User", trigger_config->get_TriggerDBConnection()->get_User());
    tdbconn.add("Password", trigger_config->get_TriggerDBConnection()->get_Password());
    tdbconn.add("Type", trigger_config->get_TriggerDBConnection()->get_Type());
    tdbconn.add("SuperMasterKey", trigger_config->get_TriggerDBConnection()->get_SuperMasterKey());
    triggerConfiguration.add_child("TriggerDBConnection."+trigger_config->get_TriggerDBConnection()->class_name(), tdbconn);
  }

  for(const auto &a : trigger_config->get_DBConnections()) {
    ptree dbconn;
    dbconn.add("UID", a->UID());
    dbconn.add("Server", a->get_Server());
    dbconn.add("Port", a->get_Port());
    dbconn.add("Name", a->get_Name());
    dbconn.add("Alias", a->get_Alias());
    dbconn.add("User", a->get_User());
    dbconn.add("Password", a->get_Password());
    dbconn.add("Type", a->get_Type());
    triggerConfiguration.add_child("DBConnections."+a->class_name(), dbconn);
  }


  // Get all ROS/ROBs
  std::map<std::string, std::vector<uint32_t> > ros2robMap;
  std::set<std::string> ros_app_types({daq::df::ROS::s_class_name});
  // For all ROS applications
  for(const auto acit : tdaq_partition.get_all_applications(&ros_app_types)) {
    /** deal with real ROS's
     * If the ROS subdetector ID is zero, 
     * - then the rob_id is expected to contain the valid subdetector ID
     * - else the ROS subdetector ID is masked in to the rob_id
     */
    if (const daq::df::ROS* ros_application = acit->cast<daq::df::ROS>()) {
      if (ros_application->disabled(tdaq_partition)) continue; //skip disabled ROS
      eformat::SubDetector subdet = static_cast<eformat::SubDetector>(ros_application->get_Detector()->get_LogicalId());
      std::vector<uint32_t> robIds;
      for(const auto rom_it : ros_application->get_Contains()) {
        if (const daq::core::ResourceSet* rom = rom_it->cast<daq::core::ResourceSet>()) {  
          if(rom->disabled(tdaq_partition)) continue; // skip disabled ROBINs
          std::vector<const daq::core::ResourceBase*> rom_resources = rom->get_Contains();
          for(const auto & jt : rom_resources ) {
            if (const daq::df::InputChannel* ic = jt->cast<daq::df::InputChannel>()) {
              if (ic->disabled(tdaq_partition)) continue; //skip disabled ROBs
              uint32_t rob_id = ic->get_Id();
              if (subdet) {
                // Set ROS subdetector ID in ROB.                
                rob_id = eformat::helper::SourceIdentifier(subdet, rob_id&0xffff).code();
              }
              robIds.push_back(rob_id);
            }
          }
        }
      }
      ros2robMap[acit->UID()] = robIds;
    }
  } // for AppConfig*

  
  ptree rosses;
  for (const auto& ros2robMapit : ros2robMap) {
    ptree r2r;
    for (size_t i=0; i < ros2robMapit.second.size(); ++i) {
      r2r.add("ROBID", ros2robMapit.second[i]);
    }
    rosses.add_child(ros2robMapit.first,r2r);
  }
  configTree.add_child("Configuration.ROS2ROBS",rosses);

  // Get DCM Application information
  std::set<std::string> dcm_app_types({daq::dcm::dal::DcmApplication::s_class_name});
  std::set<const daq::core::Computer*> hosts({daq::rc::OnlineServices::instance().getConfiguration().get<daq::core::Computer>(System::LocalHost().full_name())});
  bool dcmFound=false;
  for(const auto acit : tdaq_partition.get_all_applications(&dcm_app_types, 0, &hosts)) {
    if (const daq::dcm::dal::DcmApplication* dcm_app = acit->cast<daq::dcm::dal::DcmApplication>()) {
      dcmFound=true;
      maximumHltResultMb=dcm_app->get_sbaBlockSize_MiB();
      auto processor=dcm_app->get_processor();
      if(processor){
	eventTimeout=processor->get_processingTimeOut_ms();
      }
    }
  }
  if(!dcmFound){
    throw (daq::rc::ConfigurationIssue(ERS_HERE, "Couldn't locate DCM application"));    
  }

  // Get Application information
  ptree hltApp;
  
  hltApp.add("UID", OnlineServices::instance().getApplication().UID());
  hltApp.add("childLogRoot", OnlineServices::instance().getPartition().get_LogRoot() + "/" + OnlineServices::instance().getPartition().UID());
  hltApp.add("numForks", app->get_numForks());
  hltApp.add("numberOfAthenaMTThreads", app->get_numberOfAthenaMTThreads());
  hltApp.add("numberOfEventSlots", app->get_numberOfEventSlots());
  hltApp.add("getNextTimeout",app->get_getNextTimeout());
  hltApp.add("maximumHltResultMb", maximumHltResultMb);
  hltApp.add("finalizeTimeout", app->get_finalizeTimeout());
  hltApp.add("HardTimeout",eventTimeout);
  hltApp.add("softTimeoutFraction",app->get_softTimeoutFraction());
  for (const auto &a : app->get_extraParams()) {
    hltApp.add("extraParams.parameter", a);
  }

  // Now get the hlt configuration
  std::vector<std::string> hltLibs;
  std::string hltConfig;

  hltLibs = hlt->get_libraries();
  hltConfig=hlt->UID();

  for (size_t i=0; i< hltLibs.size(); ++i) {
    hltApp.add("HLTImplementationLibraries.library", hltLibs[i]); 
  }

  hltApp.add("DataSourceLibrary", app->get_DataSource()->get_library());   
  hltApp.add("InfoServiceLibrary", app->get_InfoService()->get_library());

  ptree dataSource;
  dataSource.add("UID", app->get_DataSource()->UID()); 
  dataSource.add("library", app->get_DataSource()->get_library()); 
  for (const auto &a : app->get_DataSource()->get_extraParams()) {
    dataSource.add("extraParams.parameter", a);
  }


  // Data from files
  if(const daq::hlt::HLTDFFileBackend* fds = app->get_DataSource()->cast<daq::hlt::HLTDFFileBackend>()) {
    dataSource.add("loopOverFiles", fds->get_loopOverFiles());
    for (const auto &a : fds->get_fileList()) {
      dataSource.add("fileList.file", a);
    }
  }

  // Data from dummy input
  if(const daq::hlt::HLTDFDummyBackend* dds = app->get_DataSource()->cast<daq::hlt::HLTDFDummyBackend>()) {
    dataSource.add("poisonPillServer", dds->get_poisonPillServer());
    dataSource.add("poisonPillName", dds->get_poisonPillName());
  }

  // See ATR-20344 - Histogram ROS Latencies
  if(app->get_ROSLatencyMeasurement() != NULL) {
    ptree rosLatencyMeasurement;
    rosLatencyMeasurement.add("maxSizeBytes", app->get_ROSLatencyMeasurement()->get_maxSizeBytes());
    rosLatencyMeasurement.add("maxDurationMicroSeconds", app->get_ROSLatencyMeasurement()->get_maxDurationMicroSeconds());
    hltApp.add_child("ROSLatencyMeasurement", rosLatencyMeasurement);
  }

  ptree infoService;
  infoService.add("UID", app->get_InfoService()->UID()); 
  infoService.add("library", app->get_InfoService()->get_library()); 
  for (const auto &a : app->get_InfoService()->get_extraParams()) {
    infoService.add("extraParams.parameter", a);
  }
  ptree crb;
  add_ConfigurationRules(app->get_InfoService()->get_ConfigurationRules(), crb);
  infoService.add_child("ConfigurationRules."+app->get_InfoService()->get_ConfigurationRules()->class_name(), crb);

  hltApp.add_child("InfoService."+app->get_InfoService()->class_name(), infoService);  
  hltApp.add_child("DataSource."+app->get_DataSource()->class_name(), dataSource);

  if(app->get_MuonCalibrationConfig() != NULL) {
    ptree circBuff;
    circBuff.add("UID", app->get_MuonCalibrationConfig()->UID());
    circBuff.add("CircName", app->get_MuonCalibrationConfig()->get_CircName());
    circBuff.add("CircSize", app->get_MuonCalibrationConfig()->get_CircSize());
    
    hltApp.add_child("MuonCalibrationConfig."+ app->get_MuonCalibrationConfig()->class_name(), circBuff);
  }

  configTree.add_child("Configuration.HLTMPPUApplication", hltApp);
    
// Now look after the HL Trigger Configuration
  
  ptree l2;
  l2.add("UID", hlt->UID()); 
  for (size_t i=0; i< hltLibs.size(); ++i) {
    l2.add("libraries.library", hltLibs[i]); 
  }

  /**
   * Generate configuration for PuDummySteering. /Per Werner
   * For the PuDummySteering and all it's related objects in OKS the full
   * OKS infomation is added to the subtree <Configuration.MPPUDummy>.
   */
  if(const daq::df::PuDummySteering *pudummy = hlt->cast<daq::df::PuDummySteering>()) {
     l2.add("resultSizeDistribution", pudummy->get_resultSizeDistribution());
     l2.add("nbrHltTriggerInfo", pudummy->get_nbrHltTriggerInfo());
     l2.add("nbrPscErrors", pudummy->get_nbrPscErrors());
     l2.add("seed", pudummy->get_seed());
         
     for (size_t ix=0; ix< pudummy->get_steps().size(); ++ix) {
	 ptree dummyStep;
	 dummyStep.add("UID",pudummy->get_steps()[ix]->UID() ); 
	 dummyStep.add("burnTimeDistribution", pudummy->get_steps()[ix]->get_burnTimeDistribution());
	 
	 ptree streamTag;
	 createStreamTag(streamTag, pudummy->get_steps()[ix]->get_debugTag());
	 dummyStep.add_child("debugTag."+pudummy->get_steps()[ix]->get_debugTag()->class_name() , streamTag);
	 streamTag.clear();
	 
	 createStreamTag(streamTag, pudummy->get_steps()[ix]->get_physicsTag());
	 dummyStep.add_child("physicsTag."+pudummy->get_steps()[ix]->get_debugTag()->class_name() , streamTag);
	 streamTag.clear();	 
	        
	 if(const daq::df::DummyCalStreamTag * cst = pudummy->get_steps()[ix]->get_calibrationTag()) {
	     createStreamTag(streamTag, cst);
	     ptree dataAcc;
	     add_DataAccess(cst->get_calibData(), dataAcc);
	     streamTag.add_child("calibData."+cst->get_calibData()->class_name(), dataAcc);
	     dummyStep.add_child("calibrationTag."+cst->class_name() , streamTag);
	 }
	 	 
	 if (const daq::df::DummyROBStep *drs = pudummy->get_steps()[ix]->cast<daq::df::DummyROBStep>()) {
	   for (size_t ix=0; ix < drs->get_roi().size(); ix++) {
	     ptree dataAcc;
	     add_DataAccess(drs->get_roi()[ix], dataAcc);
	     dummyStep.add_child("roi."+drs->get_roi()[ix]->class_name(), dataAcc);
	   }
	 }
	 l2.add_child("steps."+pudummy->get_steps()[ix]->class_name(), dummyStep);
     }
  }
  else {
    // find out if the HLT configuration uses jobOptions or the trigger data base
    const daq::hlt::HLTImplementationJobOptions* joimpl = hlt->cast<daq::hlt::HLTImplementationJobOptions>();
    const daq::hlt::HLTImplementationDB* dbimpl = hlt->cast<daq::hlt::HLTImplementationDB>();
    const daq::hlt::HLTImplementationDBPython* dbimplpy = hlt->cast<daq::hlt::HLTImplementationDBPython>();
  
    if (!joimpl && !dbimpl && !dbimplpy)
      throw (daq::rc::ConfigurationIssue(ERS_HERE,"No valid HLT implementation object found."));
   
    const daq::hlt::HLTCommonParameters* common = nullptr;
  
    if (joimpl) {
      l2.add("jobOptionsPath",joimpl->get_jobOptionsPath());
      l2.add("pythonSetupFile",joimpl->get_pythonSetupFile());
      for (size_t i=0; i< joimpl->get_logLevel().size(); ++i ) {
        l2.add("logLevels.logLevel", joimpl->get_logLevel()[i] ); 
      }
      for (size_t i=0; i< joimpl->get_preCommand().size(); ++i ) {
        l2.add("preCommands.preCommand", joimpl->get_preCommand()[i] ); 
      }
      for (size_t i=0; i< joimpl->get_postCommand().size(); ++i ) {
        l2.add("postCommands.postCommand", joimpl->get_postCommand()[i] ); 
      }
      //This is the good default for python jobOptions based runs
      l2.add("jobOptionsType","NONE");

      //      if(joimpl->get_HLTCommonParameters()) {
      //	l2.add("HLTCommonParameters", joimpl->get_HLTCommonParameters()->UID());
      //}

      //Get common parameters, if any
      common = joimpl->get_HLTCommonParameters();    
    } 
    else if (dbimplpy) {
      ers::warning(daq::rc::ConfigurationIssue(ERS_HERE, "HLT configured with HLTImplementationDBPython object. This is for debugging purposes only. Do not use this for online running!"));
      l2.add("hltPrescaleKey", dbimplpy->get_hltPrescaleKey());
      //l2.add("configureHLTMenuFrom", dbimplpy->get_configureHLTMenuFrom());
      for(const auto & a :  dbimplpy->get_additionalConnectionParameters()) {
	l2.add("additionalConnectionParameters.additionalConnectionParameter", a);
      }       
      for (const auto & a : dbimplpy->get_preCommand() ) {
        l2.add("preCommands.preCommand", a ); 
      }
      for (const auto & a : dbimplpy->get_postCommand() ) {
        l2.add("postCommands.postCommand", a ); 
      }

      //Get common parameters, if any
      common = dbimplpy->get_HLTCommonParameters();    
    }
    else if (dbimpl) {
      l2.add("hltPrescaleKey", dbimpl->get_hltPrescaleKey());
      //l2.add("configureHLTMenuFrom", dbimpl->get_configureHLTMenuFrom());
      for(const auto & a : dbimpl->get_additionalConnectionParameters()) {
	l2.add("additionalConnectionParameters.additionalConnectionParameter", a);
      }
      //Get common parameters, if any
      common = dbimpl->get_HLTCommonParameters();    
    }
    else {
      throw (daq::rc::ConfigurationIssue(ERS_HERE, "Unknown HLT Configuration type."));
    }
      
    if (common) {
      ptree comm;
      comm.add("messageSvcType", common->get_messageSvcType());
      comm.add("jobOptionsSvcType", common->get_jobOptionsSvcType());
      l2.add_child("HLTCommonParameters."+common->class_name(), comm);
    }
  }

  triggerConfiguration.add_child("hlt."+hlt->class_name(), l2);

  // Provide Partition Name
  ptree pTree;
  pTree.add("UID", OnlineServices::instance().getPartition().UID());
  std::string logs= OnlineServices::instance().getPartition().get_LogRoot()+"/"+OnlineServices::instance().getPartition().UID()+"/";
  pTree.add("LogRoot", logs );
  pTree.add_child("TriggerConfiguration."+ trigger_config->class_name(), triggerConfiguration);
  pTree.add_child("DataFlowParameters" + tdaq_partition.get_DataFlowParameters()->class_name(), dfParams);

  configTree.add_child("Configuration.Partition", pTree);
  return;
}

void daq::rc::ConfigBuilder::buildRunParams(ptree& tree) {

  // Here send rp.run_number

  // Need to add Magnet info here
  // Object type is DdcFloatInfo for both Toroid and Solenoid
  // it is composed of "ts" (Time) and "value" (Float) fields
  // Values are in initial partition DCS_GENERAL/Magnet[Toroids|Solenoid]Current.value
  // LumiBlock is in RunParams.LumiBlock object. Object Type is LumiBlock 
  try {
    IPCPartition p(OnlineServices::instance().getPartition().UID());
    RunParamsNamed rp(p, "RunParams.SOR_RunParams");
    rp.checkout();
    
    ptree pt;
    pt.put("run_number", rp.run_number);
    pt.add("max_events", rp.max_events);
    pt.add("recording_enabled", rp.recording_enabled);
    pt.add("trigger_type", rp.trigger_type);
    pt.add("run_type", rp.run_type);
    pt.add("det_mask", rp.det_mask);
    pt.add("beam_type", rp.beam_type);
    pt.add("beam_energy", rp.beam_energy);
    pt.add("filename_tag", rp.filename_tag);
    pt.add("T0_project_tag", rp.T0_project_tag);
    pt.add("timeSOR", rp.timeSOR);
    pt.add("timeEOR", rp.timeEOR);
    pt.add("totalTime", rp.totalTime);
   
    //ptree tree;
    tree.add_child("RunParams",pt);
    try{
      ptree magnets;
      IPCPartition init("initial");
      
      ddc::DdcFloatInfoNamed toroid(init, "DCS_GENERAL.MagnetToroidsCurrent.value");
      toroid.checkout();

      magnets.add("ToroidsCurrent.value", toroid.value);
      magnets.add("ToroidsCurrent.ts", toroid.ts.str());

      ddc::DdcFloatInfoNamed solenoid(init, "DCS_GENERAL.MagnetSolenoidCurrent.value");
      solenoid.checkout();

      magnets.add("SolenoidCurrent.value", solenoid.value);
      magnets.add("SolenoidCurrent.ts", solenoid.ts.str());

      tree.add_child("Magnets",magnets);
    }catch(const daq::is::Exception &ex){
      ERS_LOG("Failed to parse magnetic field currents with "<<ex);
    }catch(std::exception &ex){
      ERS_LOG("Failed to parse magnetic field currents with "<<ex.what());
    }
  }
  catch(std::exception &e) {
    daq::rc::ConfigurationIssue issue(ERS_HERE, "Building Run Params tree failed", e);
    throw issue;
  }
}

