/**
 * File: HLTRC.cxx
 * This is an implementation of the Controllable interface
 * It reads comfiguration parameters from OKS and passes them on the HLT
 * Author: G.a Lehmann Miotto
 * Date: 8.6.2012 Creation
 **/

#include <iostream>
#include <boost/property_tree/ptree.hpp>

#include <ers/ers.h>
#include <eformat/eformat.h>
#include <config/Configuration.h>
#include <dal/Partition.h>
#include <dal/ResourceBase.h>
#include <dal/ResourceSet.h>
#include <dal/RunControlApplicationBase.h>
#include <dal/Segment.h>
#include <dal/Detector.h>
#include <dal/HLTImplementation.h>
#include <dal/TriggerDBConnection.h>
#include <dal/TriggerConfiguration.h>
#include <dal/L1TriggerConfiguration.h>

/*
#include <DFdal/PTApplicationBase.h>
#include <DFdal/PtioImplementation.h>
#include <DFdal/ptioEfd.h>
#include <DFdal/ROSE.h>
*/
#include <DFdal/ROS.h>
#include <DFdal/InputChannel.h>

#include <HLTPUDal/HLTImplementationJobOptions.h>
#include <HLTPUDal/HLTImplementationDB.h>
#include <HLTPUDal/HLTImplementationDBPython.h>
#include <HLTPUDal/HLTCommonParameters.h>
#include <HLTPUDal/HLTDataSourceImpl.h>
#include <HLTPUDal/HLTMonInfoImpl.h>
#include <HLTPUDal/HLTRCApplication.h>
#include <HLTPUDal/HLTDFDummyBackend.h>
#include <HLTPUDal/HLTDFDCMBackend.h>
#include <HLTPUDal/HLTDFFileBackend.h>

#include "pudummydal/DataAccess.h"         
#include "pudummydal/DummyROBStep.h"  
#include "pudummydal/DummyStreamTag.h"
#include "pudummydal/DummyCalStreamTag.h"  
#include "pudummydal/DummyStep.h"     
#include "pudummydal/PuDummySteering.h"

#include "monsvcdal/PublishingApplication.h"
#include "monsvcdal/ConfigurationRuleBundle.h"
#include "monsvcdal/ConfigurationRule.h"
#include "monsvcdal/PublishingParameters.h"
#include "monsvcdal/OstreamPublishingParameters.h"
#include "monsvcdal/ISPublishingParameters.h"
#include "monsvcdal/OHPublishingParameters.h"

#include <rc/RunParamsNamed.h>
#include <eformat/SourceIdentifier.h>

#include "HLTRC/HLTRC.h"
#include "HLTRC/Command.h"
#include "HLTRC/HLTCommands.h"
#include "HLTRC/ConfigBuilder.h"
#include "RunControl/RunControl.h"

namespace daq {
  ERS_DECLARE_ISSUE_BASE( rc,
                          CommandForwarding,
                          rc::Exception,
                          "Failed to forward command: " << reason,
                          ERS_EMPTY,
                          ((std::string) reason)
                          )
}

using namespace boost::property_tree;


/** 
 *configure method: all parameters useful to the HLT are extracted from OKS and formatted into a command before sending the command to the child HLT
 **/
void daq::rc::HLTRC::configure(const TransitionCmd&) {
  ptree configTree; // tree of configuration parameters
  daq::rc::ConfigBuilder cfb;
  // let exceptions go through
  cfb.buildConfig(configTree);


  //Here send ptio, ros and hlt contiguration
  try {
    m_hltSender.send(daq::rc::hlt::Command(daq::rc::hlt::HLTCommands::HLT_CONFIGURE,configTree));
  }
  catch(std::exception &e) {
    daq::rc::CommandForwarding issue(ERS_HERE, e.what(), e);
    throw issue;
  }
}

void daq::rc::HLTRC::prepareForRun(const TransitionCmd&) {

  // Here send rp.run_number
  try {
    ptree tree;
    daq::rc::ConfigBuilder cfb;
    cfb.buildRunParams(tree);
    m_hltSender.send(daq::rc::hlt::Command(daq::rc::hlt::HLTCommands::HLT_START,tree));
  }
  catch(std::exception &e) {
    daq::rc::CommandForwarding issue(ERS_HERE, e.what(), e);
    throw issue;
  }
}

void daq::rc::HLTRC::stopRecording(const TransitionCmd&) {
  try {
    ptree pt;
    m_hltSender.send(daq::rc::hlt::Command(daq::rc::hlt::HLTCommands::HLT_STOP,pt));
  }
  catch(std::exception &e) {
    daq::rc::CommandForwarding issue(ERS_HERE, e.what(), e);
    throw issue;
  }
}

void daq::rc::HLTRC::unconfigure(const TransitionCmd&) {
  try {
    ptree pt;
    m_hltSender.send(daq::rc::hlt::Command(daq::rc::hlt::HLTCommands::HLT_UNCONFIGURE,pt));
  }
  catch(std::exception &e) {
    daq::rc::CommandForwarding issue(ERS_HERE, e.what(), e);
    throw issue;
  }
}

void daq::rc::HLTRC::user(const UserCmd &cmd) {

  // Understood commands are FORK(count), KILLALL(), KILL(childname)
  ptree configTree;
  ptree pt;
  pt.add("COMMANDNAME", cmd.commandName());
  std::ostringstream arg;
  for (const auto & a : cmd.commandParameters()) {
    arg<< " " <<a;
  }

  if (cmd.commandName() == "FORK") {
    pt.add("COUNT", arg.str());
  }
  else if (cmd.commandName() == "FORK_AFTER_CRASH") {
  }
  else if (cmd.commandName()== "KILL") {
    pt.add("CHILDNAME", arg.str());
  }
  else if (cmd.commandName()=="KILLALL") {
  }
  else { //unknown command, just pass on arguments
    pt.add("ARG", arg.str());
  }

  configTree.add_child("Configuration", pt);

  try {
    m_hltSender.send(daq::rc::hlt::Command(daq::rc::hlt::HLTCommands::HLT_USERCMD,configTree));
  }
  catch(std::exception &e) {
    daq::rc::CommandForwarding issue(ERS_HERE, e.what(), e);
    throw issue;
  }

}

void daq::rc::HLTRC::onExit(daq::rc::FSM_STATE s) noexcept {
  ERS_LOG("Exiting " << OnlineServices::instance().applicationName() << " from state " << daq::rc::FSMStates::stateToString(s) );
}
